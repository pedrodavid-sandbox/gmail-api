
import com.google.api.client.auth.oauth2.Credential
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets
import com.google.api.client.googleapis.batch.json.JsonBatchCallback
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport
import com.google.api.client.googleapis.json.GoogleJsonError
import com.google.api.client.http.HttpHeaders
import com.google.api.client.http.javanet.NetHttpTransport
import com.google.api.client.json.JsonFactory
import com.google.api.client.json.gson.GsonFactory
import com.google.api.client.util.store.FileDataStoreFactory
import com.google.api.services.gmail.Gmail
import com.google.api.services.gmail.GmailScopes
import com.google.api.services.gmail.model.ListThreadsResponse
import com.google.api.services.gmail.model.Message
import com.google.api.services.gmail.model.MessagePart
import com.google.api.services.gmail.model.Thread
import net.sourceforge.tess4j.ITessAPI
import net.sourceforge.tess4j.ITesseract
import net.sourceforge.tess4j.Tesseract
import net.sourceforge.tess4j.TesseractException
import java.awt.image.BufferedImage
import java.io.*
import java.nio.file.Files
import java.security.GeneralSecurityException
import java.util.*
import kotlin.system.measureTimeMillis


/* class to demonstrate use of Gmail list labels API */
object GmailQuickstart {
    /**
     * Application name.
     */
    private const val APPLICATION_NAME = "Gmail API Java Quickstart"

    /**
     * Global instance of the JSON factory.
     */
    private val JSON_FACTORY: JsonFactory = GsonFactory.getDefaultInstance()

    /**
     * Directory to store authorization tokens for this application.
     */
    private const val TOKENS_DIRECTORY_PATH = "tokens"

    /**
     * Global instance of the scopes required by this quickstart.
     * If modifying these scopes, delete your previously saved tokens/ folder.
     */
    private val SCOPES = listOf(GmailScopes.GMAIL_LABELS, GmailScopes.GMAIL_READONLY)
    private const val CREDENTIALS_FILE_PATH = "/client_secret.apps.googleusercontent.com.json"

    /**
     * Creates an authorized Credential object.
     *
     * @param HTTP_TRANSPORT The network HTTP Transport.
     * @return An authorized Credential object.
     * @throws IOException If the credentials.json file cannot be found.
     */
    @Throws(IOException::class)
    private fun getCredentials(HTTP_TRANSPORT: NetHttpTransport): Credential {
        // Load client secrets.
        val `in` =
            GmailQuickstart::class.java.getResourceAsStream(CREDENTIALS_FILE_PATH)
                ?: throw FileNotFoundException("Resource not found: " + CREDENTIALS_FILE_PATH)
        val clientSecrets =
            GoogleClientSecrets.load(
                JSON_FACTORY,
                InputStreamReader(`in`)
            )

        // Build flow and trigger user authorization request.
        val flow =
            GoogleAuthorizationCodeFlow.Builder(
                HTTP_TRANSPORT, JSON_FACTORY, clientSecrets, SCOPES
            )
                .setDataStoreFactory(
                    FileDataStoreFactory(
                        File(
                            TOKENS_DIRECTORY_PATH
                        )
                    )
                )
                .setAccessType("offline")
                .build()
        val receiver = LocalServerReceiver.Builder().setPort(8888).build()
        //returns an authorized Credential object.
        return AuthorizationCodeInstalledApp(flow, receiver).authorize("user")
    }

    @Throws(IOException::class, GeneralSecurityException::class)
    fun test() {
        // Build a new authorized API client service.
        val HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport()
        val service = Gmail.Builder(HTTP_TRANSPORT, JSON_FACTORY, getCredentials(HTTP_TRANSPORT))
            .setApplicationName(APPLICATION_NAME)
            .build()

        // Print the labels in the user's account.
        val user = "me"
//        listLabels(service, user)
        val query = "label:01---finances-00---todo"
//        getThreadsByQuery(service, user, query).forEach { thread ->
//            println(thread.subject())
//        }

        // This is the actual label, but we will use the query instead
        val label = "Label_9071203710389076266"
//        getThreadsByLabel(service, user, label).forEach { thread ->
//            println(thread.subject())
//        }
//        val timeInMillis = measureTimeMillis {
//            getThreadsByLabel(service, user, label).forEach { thread ->
////                println(thread.subject())
//            }
//        }
//        println("Took $timeInMillis")
        val timeInMillis2 = measureTimeMillis {
            getThreadsBatch(service, user, label)
        }
        println("Took $timeInMillis2")
    }

    private fun listLabels(service: Gmail, user: String) {
        val listResponse = service.users().labels().list(user).execute()
        val labels = listResponse.labels
        if (labels.isEmpty()) {
            println("No labels found.")
        } else {
            println("Labels:")
            for (label in labels) {
                println("- ${label.name} (${label.id})")
            }
        }
    }

    private fun Thread.subject(): String {
        return messages[0]
            .payload
            .headers
            .find { it.name == "Subject" }
            ?.value
            ?: "(no Subject)"
    }

    private fun Thread.date(): String {
        return messages[0]
            .payload
            .headers
            .find { it.name == "Date" }
            ?.value
            ?: "(no Date)"
    }

    private fun Thread.from(): String {
        return messages[0]
            .payload
            .headers
            .find { it.name == "From" }
            ?.value
            ?: "(no From)"
    }

    private fun getSubject(service: Gmail, user: String, thread: Thread): String {
        val threadResp = service.users().threads().get(user, thread.id).execute()
        return threadResp.subject()
    }

    private fun getEmails(service: Gmail, user: String) {
        val query = "label:01---finances-00---todo"

        println("Page:")
        val threadsResp = service.users().threads().list(user).setQ(query)
            .execute()
        for (thread in threadsResp.threads) {
            val subject = getSubject(service, user, thread)
            println("\t$subject")
        }
        while (threadsResp.nextPageToken != null) {
            println("Page:")
            val followingThreadsResp = service.users().threads().list(user).setQ(query)
                .setPageToken(threadsResp.nextPageToken)
                .execute()
            for (thread in followingThreadsResp.threads) {
                val subject = getSubject(service, user, thread)
                println("\t$subject")
            }
        }
    }

    private fun getThreadsByQuery(service: Gmail, user: String, query: String) = sequence {
        val threadsResp = service.users().threads().list(user).setQ(query)
            .execute()
        for (thread in threadsResp.threads) {
            val threadResp = service.users().threads().get(user, thread.id).execute()
            yield(threadResp)
        }
        var nextPageToken = threadsResp.nextPageToken
        while (nextPageToken != null) {
            println("Page:")
            val followingThreadsResp = service.users().threads().list(user).setQ(query)
                .setPageToken(nextPageToken)
                .execute()

            nextPageToken = followingThreadsResp.nextPageToken
            for (thread in followingThreadsResp.threads) {
                val threadResp = service.users().threads().get(user, thread.id).execute()
                yield(threadResp)
            }
        }
    }

    private fun getThreadsByLabel(service: Gmail, user: String, label: String) = sequence {
        val threadsResp = service.users().threads().list(user).setLabelIds(listOf(label))
            .execute()
        for (thread in threadsResp.threads) {
            val threadResp = service.users().threads().get(user, thread.id).execute()
            yield(threadResp)
        }
        var nextPageToken = threadsResp.nextPageToken
        while (nextPageToken != null) {
            println("Page:")
            val followingThreadsResp = service.users().threads().list(user).setLabelIds(listOf(label))
                .setPageToken(nextPageToken)
                .execute()

            nextPageToken = followingThreadsResp.nextPageToken
            for (thread in followingThreadsResp.threads) {
                val threadResp = service.users().threads().get(user, thread.id).execute()
                yield(threadResp)
            }
        }
    }

    private fun processParts(service: Gmail, user: String, t: Thread, message: Message, parts: List<MessagePart>, additionalTabs: Int = 0) {
        parts.forEach { part ->
            val mimeType = part.mimeType
            println("\t\t\t${"\t".repeat(additionalTabs)} - $mimeType")
            if (mimeType.startsWith("multipart")) {
                processParts(service, user, t, message, part.parts ?: throw IllegalStateException("Message has multipart mimeType ('$mimeType') but has no parts"), additionalTabs + 1)
            }
            if (part.body.attachmentId != null) {
                val attachment =
                    service.users().messages().attachments().get(user, message.id, part.body.attachmentId)
                        .execute()
                val attachmentData = attachment.decodeData()
                val partOfFilename = part.filename.ifEmpty {
                    val extension = when(mimeType) {
                        "image/png" -> "png"
                        "image/jpg" -> "jpg"
                        else -> throw IllegalStateException("Unexpected extension")
                    }
                    "${part.partId}.$extension"
                }
                val filename = "/Users/pedro.david/Downloads/email-tests/${t.subject()}/${partOfFilename}"
                File(filename).parentFile.mkdirs()
                Files.write(File(filename).toPath(), attachmentData)
            }
            if (arrayOf("text/html", "text/plain").any { it == mimeType }) {
                val extension = when(mimeType) {
                    "text/html" -> "html"
                    "text/plain" -> "txt"
                    else -> throw IllegalStateException("This won't happen")
                }
                val filename = "/Users/pedro.david/Downloads/email-tests/${t.subject()}/part_${part.partId}.${extension}"
                File(filename).parentFile.mkdirs()
                Files.write(File(filename).toPath(), part.body.decodeData())
                tryFindValues(part.body.decodeData())
            }
        }
    }
    private fun processTextHtml(service: Gmail, user: String, t: Thread, message: Message) {
        val pathName = "/Users/pedro.david/Downloads/email-tests/${t.subject()}/index.html"
        val file = File(pathName)
        file.parentFile.mkdirs()
        val payloadData = message.payload.body.decodeData()
        Files.write(file.toPath(), payloadData)
        tryFindValues(payloadData)
    }

    private fun tryFindValues(payloadData: ByteArray) {
        val payload = String(payloadData)

        payload.findAnyOf(listOf("Total", "€"))
        val indices = Regex("€|£|\$").findAll(payload).map {
            it.range.first
        }.toList()
        Regex("Total:?(\\s|&nbsp;)*([€|£|\$]?(\\s|&nbsp;)*\\d+[,.]\\d+(\\s|&nbsp;)*[€|£|\$]?)")
        val values = Regex("Total:?\\s+([€|£|\$]?\\d+[,.]\\d+\\s?[€|£|\$]?)").findAll(payload).map { it.value }.toList()
        Regex("Total\\s+([€|£|\$]?\\d+[,.]\\d+\\s?[€|£|\$]?)")
        println(indices)
    }

    private fun getThreadsBatch(service: Gmail, user: String, label: String) {
        val batch = service.batch()
        val threadBatch = service.batch()
        var counter = 0

        val threadCallback = object : JsonBatchCallback<Thread>() {
            override fun onSuccess(t: Thread, responseHeaders: HttpHeaders) {
                println("\t- ${counter++} | ${t.date()} | ${t.subject()} | ${t.from()}")
                t.messages.forEach { message ->
                    println("\t\t - ${message.payload.mimeType}")
                    when(val mimeType = message.payload.mimeType) {
                        "multipart/alternative", "multipart/mixed", "multipart/related" -> {
                            processParts(service, user, t, message, message.payload.parts ?: throw IllegalStateException("Message has multipart mimeType ('$mimeType') but has no parts"))
                        }
                        "text/html" -> processTextHtml(service, user, t, message)
                        else -> throw IllegalStateException("We're not ready to handle Messages with mimeType '$mimeType'")
                    }
                }
            }

            override fun onFailure(e: GoogleJsonError, responseHeaders: HttpHeaders) {
            }
        }

        val callback = object : JsonBatchCallback<ListThreadsResponse>() {
            override fun onSuccess(threadsResp: ListThreadsResponse, responseHeaders: HttpHeaders) {
                for (thread in threadsResp.threads) {
                    service.users().threads().get(user, thread.id)
                        .queue(threadBatch, threadCallback)
                }
            }
            override fun onFailure(e: GoogleJsonError, responseHeaders: HttpHeaders) {
                println("Error Message: ${e.message}")
            }
        }
        service.users().threads().list(user).setLabelIds(listOf(label))
            .queue(batch, callback)

        batch.execute()
        threadBatch.execute()

        /**
         * TODO: - Pagination
         *       - Attachments in Batch Request
         *       - Identify items / quantities from html (e.g.: PCDiga emails are HTML only), text (seems potentially easier) and attachments (mostly pdfs)
         */
    }

    private fun test(filename: String) {
        // https://github.com/nguyenq/tess4j/issues/194
        System.setProperty("jna.library.path", "/opt/homebrew/Cellar/tesseract/5.2.0/lib/")

        val instance: ITesseract = Tesseract() // JNA Interface Mapping
        // ITesseract instance = new Tesseract1(); // JNA Direct Mapping

        instance.setDatapath("/opt/homebrew/Cellar/tesseract/5.2.0/share/tessdata/")
//        instance.setDatapath("/opt/homebrew/Cellar/tesseract-lang/4.1.0/share/tessdata/") // path to tessdata directory

        try {
//            instance.setOcrEngineMode(4)
            // instance.getSegmentedRegions()
            instance.getWords(null as BufferedImage?, ITessAPI.TessPageIteratorLevel.RIL_TEXTLINE)
            val result: String = instance.doOCR(File(filename))
            println(result)
        } catch (e: TesseractException) {
            System.err.println(e.message)
        }
    }
}